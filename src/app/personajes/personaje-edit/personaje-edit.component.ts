import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';
import { EstadoFormEnum } from '../../shared/estadoForm.enum';

import { NavbarComponent } from '../../navbar/navbar.component';

import { Observable } from 'rxjs/Observable';
import { PersonajeService} from '../../shared/personaje-service';

import { PersonajeModel } from '../../shared/personajeForm.model';
import { FirebasePostResponseModel } from '../../shared/firebase-response.model';

@Component({
  selector: 'heroes-api-personaje-edit',
  templateUrl: './personaje-edit.component.html'
})
export class PersonajeEditComponent implements OnInit {

  public pattern = /^\d{4}\.\d{1,2}\.\d{1,2}$/;

  public estadoFormEnum = EstadoFormEnum;
  public estadoForm: EstadoFormEnum = EstadoFormEnum.estadoInicial;

  public heroeForm: FormGroup = new FormGroup({
    "nombre": new FormControl("", Validators.required),
    "tipo": new FormControl(""),
    "bio": new FormControl(""),
    "aparicion": new FormControl("", [Validators.required]),
    "imagen": new FormControl(null)
  })

  public id:string;

  public personaje :{key: string, personaje: PersonajeModel};

  constructor(private _activatedRoute: ActivatedRoute,
              private _router: Router,
              private _personajeService: PersonajeService,
              private _location: Location) { }

  ngOnInit(){
    this.heroeForm.reset(this.personaje);
    this._activatedRoute.params
     .switchMap( params => {
       this.id = params['id'];
       if (this.id == "nuevo") {
         return Observable.of(new PersonajeModel);
       } else {
         return this._personajeService.obtenerPersonaje(this.id);
       }
     })
     .subscribe( (heroe: PersonajeModel) => {
       this.heroeForm.reset(heroe);
     });
  }

  public verImagen(imagen){
    return imagen.url;
  }

  public guardar(){
    if(this.heroeForm.valid){
      this.estadoForm = EstadoFormEnum.enviando;
      if(this.heroeForm.value.imagen == null || this.heroeForm.value.imagen == null ){
        this.heroeForm.value.imagen = "assets/img/noimage.png";
      }
      if(this.id === "nuevo"){
        this.altaHeroe();
      }else{
        this.actualizarPersonaje();
      }

    }else{
      console.log("Te faltan datos para introducir");
      this.estadoForm = EstadoFormEnum.formInvalido;
    }
  }

  public altaHeroe(){
    this._personajeService.nuevoPersonaje(this.heroeForm.value).subscribe(
      (datos: FirebasePostResponseModel)   => {
      this.estadoForm = EstadoFormEnum.envioOkPost;
      this._router.navigate(['/personaje', datos.name])
    },
    error => {
      console.log("Error al guardar heroe", error)
      this.estadoForm = EstadoFormEnum.envioError;
    });
  }

  public actualizarPersonaje(){
    this._personajeService.actualizarPersonaje(this.heroeForm.value, this.id)
      .subscribe( (datos: PersonajeModel) =>{
        this.estadoForm = EstadoFormEnum.envioOkPut;
      },
    error => {
      console.error("Error al actualizar Personaje", error);
      this.estadoForm = EstadoFormEnum.envioError;
    })
  }

  public volver(){
    this._personajeService.volver();
  }
}
