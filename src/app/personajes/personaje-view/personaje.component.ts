import { Component, OnInit, Input} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';

import { Observable } from 'rxjs/Observable';
import { PersonajeService} from '../../shared/personaje-service';

import { PersonajeModel } from '../../shared/personajeForm.model';



@Component({
  selector: 'heroes-api-personaje',
  templateUrl: './personaje.component.html'
})
export class PersonajeViewComponent implements OnInit {

  @Input('personaje') personaje: PersonajeModel = null;

  public id: string;

  constructor(private _activatedRoute: ActivatedRoute,
              private _router: Router,
              private _personajeService: PersonajeService,
              private _location: Location) { }

  ngOnInit(){
    if(this.personaje){

    }else{
      this._activatedRoute.params
       .switchMap( params => {
         this.id = params['id'];
         if (this.id == "nuevo") {
           return Observable.of(new PersonajeModel);
         } else {
           return this._personajeService.obtenerPersonaje(this.id);
         }
       })
       .subscribe( (heroe: PersonajeModel) => {
         this.personaje = heroe;
       });
     }
  }

  public editarPersonaje(){
    this._router.navigate(['/personaje/edit', this.id])
  }

  public borrarPersonaje(){
    this._personajeService.borrarPersonaje(this.id)
      .subscribe(res => {
        if (res == null){
          delete this.id;
        }else{
          console.error("Error al eliminar Villano", res)
        }
      }, error => {
        console.error("Error al eliminar Villano", error);
      });
    this._router.navigate(['/'+this.personaje.tipo+'s'])
  }

  public volver(){
    this._personajeService.volver();
  }
}
