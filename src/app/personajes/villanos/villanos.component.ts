import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';

import { PersonajeService} from '../../shared/personaje-service';

import { PersonajeModel } from '../../shared/personajeForm.model';

@Component({
  selector: 'heroes-api-villanos',
  templateUrl: './villanos.component.html'
})
export class VillanosComponent implements OnInit {

  public listaVillanos: {key: string, personaje:PersonajeModel}[] = [];
  public ejecutarPipe: boolean= false;

  constructor(private _activatedRoute: ActivatedRoute,
              private _router: Router,
              private _personajeService: PersonajeService) { }

  ngOnInit() {

    if(this._personajeService.listaPersonajes.length == 0 ||
                  !this._personajeService.listaPersonajes){

      this._personajeService.obtenerPersonajes().subscribe(() => {
        this._personajeService.filtrarTipo();
        this.listaVillanos = this._personajeService.listaVillanos;
      })
    }else{
      this.listaVillanos = this._personajeService.listaVillanos;
    }

  }

  public borrarVillano(key: string){
    this._personajeService.borrarPersonaje(key)
      .subscribe(res => {
        if (res == null){
          delete this.listaVillanos[key];
          this.ejecutarPipe = !this.ejecutarPipe;
        }else{
          console.error("Error al eliminar Villano", res)
        }
      }, error => {
        console.error("Error al eliminar Villano", error);
      });
  }

  public editHeroe(key: string){
    this._router.navigate(['/personaje/edit', key]);
  }

  public verDetalle(key: string){
    this._router.navigate(['/personaje', key]);
  }

}
