import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keyValor'
})
export class KeyValorPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(value == null){
      return [];
    }
    return Object.keys(value);
  }

}
