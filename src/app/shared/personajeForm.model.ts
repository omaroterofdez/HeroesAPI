export class PersonajeModel{
  nombre: string;
  tipo: string;
  bio: string;
  imagen: string;
  aparicion: Date;

  constructor(){
    this.nombre = "";
    this.tipo= "heroe";
    this.bio= "";
    this.aparicion = null;
    this.imagen= "";
  }
}
