export enum EstadoFormEnum{
  estadoInicial,
  enviando,
  envioOkPut,
  envioOkPost,
  envioError,
  formValido,
  formInvalido
}
