import { Injectable } from '@angular/core';

import { Location } from '@angular/common'

import { Router } from '@angular/router';

import { PersonajeModel } from './PersonajeForm.model';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class PersonajeService {
  private personajesURL: string= "https://heroesapi-bc10c.firebaseio.com/personajes.json";
  private obtenPersonajeURL: string="https://heroesapi-bc10c.firebaseio.com/personajes/";

  public listaPersonajes: {key: string, personaje:PersonajeModel}[] = [];
  public listaHeroes: {key: string, personaje:PersonajeModel}[] = [];
  public listaVillanos: {key: string, personaje:PersonajeModel}[] = [];

  public list: {key: string, personaje:PersonajeModel}[];

  public tempUrl: string;

  constructor(private _http: Http, private _router: Router, private _location: Location) {

  }

  public nuevoPersonaje(personaje: PersonajeModel){
    let body: string =  JSON.stringify(personaje);
    let headers: Headers = new Headers({
      "Content-type": "application/json"
    });
      return this._http.post(this.personajesURL, body, {headers: headers })
        .map((res: Response)  => {
          return res.json();
      })
  }

  public actualizarPersonaje(personaje: PersonajeModel, key:string){
    let body: string =  JSON.stringify(personaje);
    let headers: Headers = new Headers({
      "Content-type": "application/json"
    });
    let url=`${this.obtenPersonajeURL}/${key}.json`;
      return this._http.put( url , body, {headers: headers })
        .map((res: Response)  => {
          return res.json();
      })
  }

  public obtenerPersonaje(key: string){
    let headers: Headers = new Headers({
      "Content-type": "application/json"
    });
    let url=`${this.obtenPersonajeURL}/${key}.json`;
      return this._http.get( url , {headers: headers })
        .map((res: Response)  => {
          return res.json();
      })
  }

  public obtenerPersonajes(){
    this.listaPersonajes = [];
    this.listaHeroes = [];
    this.listaVillanos = [];
    this.tempUrl = this._router.url;
    let headers: Headers = new Headers({
      "Content-type": "application/json"
    });
    let url=`${this.obtenPersonajeURL}.json`;
      return this._http.get( url , {headers: headers })
        .map((res: Response)  => {
          this.listaPersonajes = res.json();
          return res.json();
      })
  }

  public filtrarTipo(){
    let claves = Object.keys(this.listaPersonajes);
    for( let key of claves){
      let personaje = this.listaPersonajes[key];
      if(personaje.tipo == 'heroe'){
        this.listaHeroes.push({key, personaje})
      }else{
        this.listaVillanos.push({key, personaje})
      }
    }
  }


  public borrarPersonaje(key: string){
    let headers: Headers = new Headers({
      "Content-type": "application/json"
    });
    let url=`${this.obtenPersonajeURL}/${key}.json`;
      return this._http.delete( url , {headers: headers })
        .map((res: Response)  => {
          return res.json();
      })
  }

  public buscarPersonaje(buscarTexto: string){
    this.list = [];
    let claves= Object.keys(this.listaPersonajes);
    for (let clave of claves) {
      let name: string = this.listaPersonajes[clave].nombre;
      if(name.toLowerCase().includes(buscarTexto.toLowerCase())){
        this.list[clave] = this.listaPersonajes[clave];
      }
    }
    return this.list;
  }

  public volver(){
    this._location.back();
  }

}
