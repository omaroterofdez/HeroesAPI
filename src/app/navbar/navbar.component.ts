import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';


import { PersonajeService} from '../shared/personaje-service';

@Component({
  selector: 'heroes-api-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  private pulsado: boolean;
  private refresh: string;

  constructor(private _activatedRoute: ActivatedRoute,
              private _router: Router,
              private _personajeService: PersonajeService) { }

  ngOnInit() {
  }

  public buscarHeroe(buscarTexto:string){
    this._router.navigate(['search', buscarTexto]);
  }

  public refreshList(){
    this.pulsado = true
    this.refresh = "2";
    this._router.navigate(['home']);
    this._personajeService.obtenerPersonajes()
      .subscribe( (datos) =>{
      this.refresh = "1";
      this._router.navigate([this._personajeService.tempUrl]);
      setTimeout(()=> this.pulsado = false,3000);
      },
    error => {
      this.refresh = "0";
    })
  }

}
