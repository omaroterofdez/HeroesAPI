import { RouterModule, Routes } from '@angular/router';

import { HeroesComponent } from './personajes/heroes/heroes.component'
import { VillanosComponent } from './personajes/villanos/villanos.component'
import { PersonajeEditComponent } from './personajes/personaje-edit/personaje-edit.component';
import { PersonajeViewComponent } from './personajes/personaje-view/personaje.component';
import { SearchComponent } from './search/search.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component'

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'heroes', component: HeroesComponent},
  { path: 'villanos', component: VillanosComponent},
  { path: 'about', component: AboutComponent},
  { path: 'personaje/edit/:id', component: PersonajeEditComponent },
  { path: 'personaje/:id', component: PersonajeViewComponent },
  { path: 'search', component: SearchComponent},
  { path: 'search/:buscarTexto', component: SearchComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
