import { Component, OnInit } from '@angular/core';

import { PersonajeService} from './../shared/personaje-service';

@Component({
  selector: 'heroes-api-about',
  templateUrl: './about.component.html'
})
export class AboutComponent implements OnInit {

  constructor(private personajeService: PersonajeService) { }

  ngOnInit() {
  }

}
