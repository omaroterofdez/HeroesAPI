import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PersonajeViewComponent } from './personajes/personaje-view/personaje.component';
import { PersonajeEditComponent } from './personajes/personaje-edit/personaje-edit.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SearchComponent } from './search/search.component';
import { HeroesComponent } from './personajes/heroes/heroes.component';

import { APP_ROUTING } from './app.routes';

import { PersonajeService } from './shared/personaje-service';
import { ClaveValorPipe } from './shared/clave-valor.pipe';
import { KeyValorPipe } from './shared/key-valor.pipe';
import { VillanosComponent } from './personajes/villanos/villanos.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonajeViewComponent,
    ClaveValorPipe,
    KeyValorPipe,
    PersonajeEditComponent,
    NavbarComponent,
    SearchComponent,
    HeroesComponent,
    VillanosComponent,
    HomeComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    APP_ROUTING
  ],
  providers: [PersonajeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
