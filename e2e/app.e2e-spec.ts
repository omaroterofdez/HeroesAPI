import { FHeroesAPIPage } from './app.po';

describe('f-heroes-api App', () => {
  let page: FHeroesAPIPage;

  beforeEach(() => {
    page = new FHeroesAPIPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
